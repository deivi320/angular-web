import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { FiltroService } from "../services/filtrar/filtrar.service";
import { FormControl, FormGroup } from "@angular/forms";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { AngularFireAuth } from "@angular/fire/auth/";


@Component({
  selector: "app-barra-navegacion",
  templateUrl: "./barra-navegacion.component.html",
  styleUrls: ["./barra-navegacion.component.css"],
})
export class BarraNavegacionComponent implements OnInit {

  nombre: FormControl;
  apellido: FormControl;
  genero: FormControl;
  edad: FormControl;
  correo: FormControl;
  numeroDeTelefono: FormControl;
  numeroDeDocumento: FormControl;
  tipoDeDocumento: FormControl;
  rol: FormControl;
  usuarios: FormGroup;
  userId: string = "";
  datosActualizar: Array<any> = [];
  variable:string="pancho";



  actualizarUsuarioControl() {
    this.nombre = new FormControl("");
    this.apellido = new FormControl("");
    this.genero = new FormControl("");
    this.edad = new FormControl("");
    this.correo = new FormControl("");
    this.numeroDeTelefono = new FormControl("");
    this.numeroDeDocumento = new FormControl("");
    this.tipoDeDocumento = new FormControl("");
    this.rol = new FormControl("");
  }

  crearActualizarUsuario() {
    this.usuarios = new FormGroup({
      nombre: this.nombre,
      apellido: this.apellido,
      genero: this.genero,
      edad: this.edad,
      correo: this.correo,
      numeroDeTelefono: this.numeroDeTelefono,
      numeroDeDocumento: this.numeroDeDocumento,
      tipoDeDocumento: this.tipoDeDocumento,
      rol: this.rol,
    });
  }

  ngOnInit() {
    
    this.actualizarUsuarioControl();
    this.crearActualizarUsuario();
    this.auth.authState.subscribe((user) => {
      if (user) {
        this.userId = user.uid;
        this.buscarId();
      }
      
    });
  
 
  }

  //-----end poits  y metodos para conectar con el back -------------------
  API_URL: string = "http://localhost:8080/api/";
  URLUPDATE: string = "http://localhost:8080/api/users";

  //------metodo para consultar por el uid del usurio----------------------

  leerUsuario(usuarioId): Observable<any> {
    return this.http.get<any>(
      "http://localhost:8080/api/usuario/usuarios/" + usuarioId
    );
  }

  //---------metodo para actualizar el suario.--------------------------------
  updatePersona(data: any): Observable<any> {
    return this.http.put<any>(
      "http://localhost:8080/api/users/" + data.numeroDeDocumento,
      data
    );
  }

  //----------metodo para lista los datos del,  usuario por uid en la ventana modal.
 
  buscarId() {
    this.leerUsuario(this.userId).subscribe((data: any[]) => {
      console.log(data);
      this.datosActualizar = data;
      console.log(this.datosActualizar)
      console.log("arreglo de usuarios ");
      console.log(this.datosActualizar)
    });
  }
 

  onSubmit() {
    this.updatePersona({
      nombre: this.nombre.value,
      apellido: this.apellido.value,
      genero: this.genero.value,
      edad: this.edad.value,
      correo: this.correo.value,
      numeroDeTelefono: this.numeroDeTelefono.value,
      numeroDeDocumento: this.numeroDeDocumento.value,
      tipoDeDocumento: this.tipoDeDocumento.value,
      rol: this.rol.value,
    }).subscribe((res) => console.info(res));
    alert('SUCCESS!! :-)');
    this.ngOnInit();
  }

  constructor(

    public auth: AngularFireAuth,
    private router: Router,
    private usuarioInyectado: FiltroService,
    private http: HttpClient
  ) {}
}
