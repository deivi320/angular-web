import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {UserProfileComponent} from './user-profile/user-profile.component';
import {FormularioComponent} from './formulario/formulario.component';
import { AuthGuard } from "./services/auth.guard";
import {RegisterComponent} from './register/register.component';
import {BuscadorComponent} from './buscador/buscador.component';
import { InicioComponent } from './inicio/inicio.component';
import { DatosPersonalesComponent } from './datos-personales/datos-personales.component';
import { AdminComponent } from './admin/admin.component';

const routes: Routes = [
  {path: 'login', component: UserProfileComponent},
  {path: 'register', component: RegisterComponent,canActivate : [AuthGuard]},
  {path: 'inicio', component: InicioComponent ,canActivate : [AuthGuard]},
  {path: 'datosPersonales', component: DatosPersonalesComponent ,canActivate : [AuthGuard]},
  {path: 'hojaDeVida', component:FormularioComponent,canActivate : [AuthGuard]},
  {path: 'search', component: BuscadorComponent,canActivate : [AuthGuard]},
  {path: 'admin', component: AdminComponent,canActivate : [AuthGuard]},
  {path: '', redirectTo: '/login', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
