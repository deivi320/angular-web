import {Component, OnInit, Input} from '@angular/core';
import {Habilidades} from '../models/habilidades.models';
import {HabilidadesService} from '../services/habilidades/habilidades.service';

@Component({
  selector: 'app-listar-habilidades',
  templateUrl: './listar-habilidades.component.html',
  styleUrls: ['./listar-habilidades.component.css']
})
export class ListarHabilidadesComponent implements OnInit {
  @Input() usuarioId: string;
  habilidades: Habilidades[];

  constructor(private service: HabilidadesService) {
  }

  ngOnInit(): void {
    this.service.leerRegistros(this.usuarioId['id']).subscribe((habilidadesDesdeApi) => {
      this.habilidades = habilidadesDesdeApi;
    });
  }

}
