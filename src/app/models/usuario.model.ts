export class Usuario {
  usuarioId: string;
  nombre: string;
  apellido: string;
  genero: string;
  edad: number;
  correo: string;
  numeroDeTelefono: string;
  numeroDeDocumento: string;
  tipoDeDocumento: string;
  rol: string;
}
