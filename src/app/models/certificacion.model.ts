export class Certificacion {
  usuarioId: string;
  idCertificacion: string;
  nombre: string;
  institucion: string;
  periodo: string;
}
