import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  private SOFKA_LOGO =
    'assets/img/logo_sofka.png';

  constructor() { }

  ngOnInit(): void {
  }

  getLogo() {
    return this.SOFKA_LOGO;
  }

}
