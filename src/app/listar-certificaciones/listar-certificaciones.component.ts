import { Component, OnInit, Input } from "@angular/core";
import { Certificacion } from "../models/certificacion.model";
import { CertificacionesService } from "../services/certificaciones/certificaciones.service";

@Component({
  selector: "app-listar-certificaciones",
  templateUrl: "./listar-certificaciones.component.html",
  styleUrls: ["./listar-certificaciones.component.css"],
})
export class ListarCertificacionesComponent implements OnInit {
  @Input() usuarioId: string;

  certificaciones: Certificacion[];

  constructor(private service: CertificacionesService) {}

  ngOnInit(): void {
    this.service
      .leerRegistros(this.usuarioId["id"])
      .subscribe((certificacionesDesdeApi) => {
        this.certificaciones = certificacionesDesdeApi;
      });
  }
}
