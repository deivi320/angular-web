import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarCertificacionesComponent } from './listar-certificaciones.component';

describe('ListarCertificacionesComponent', () => {
  let component: ListarCertificacionesComponent;
  let fixture: ComponentFixture<ListarCertificacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarCertificacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarCertificacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
