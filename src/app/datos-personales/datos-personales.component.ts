import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-datos-personales',
  templateUrl: './datos-personales.component.html',
  styleUrls: ['./datos-personales.component.css']
})
export class DatosPersonalesComponent implements OnInit {

  datosPersonales1: object =
  {
    nombre: 'Pacho',
    apellido:  'Gómez',
    tipoDocumento: 'Cédula de Ciudadanía',
    numeroDocumento: '1036664587',
    genero: 'Masculino',
    edad: '24',
    telefono: '3146816260',
    correo: 'pacho_xp@hotmail.com'
  };

  tiposDocumento: Array<string> = 
  [
    'Tarjeta de identidad',
    'Cédula de ciudadanía',
    'Cédula de extrangería'
  ];

  generos: Array<string> =
  [
    'Masculino',
    'Femenino'
  ];

  datosPersonales: FormGroup;

  esNuevo: boolean = true;
  posicionEditar: number = -1;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.crearDatosPersonales();
  }

  crearDatosPersonales()
  {
    this.datosPersonales = this.formBuilder.group(
      {
        nombre: [ '', Validators.required ],
        apellido: [ '', Validators.required],
        tipoDocumento: [ '', Validators.required],
        numeroDocumento: [ '', Validators.required],
        genero: [ '', Validators.required ],
        edad: [ '', Validators.required ],
        telefono: [ '', Validators.required ],
        correo: [ '', Validators.compose([ 
          Validators.required, Validators.email
        ]) ],
      })
  }

  cargar(){
    this.datosPersonales.reset(this.datosPersonales1);
  }
}