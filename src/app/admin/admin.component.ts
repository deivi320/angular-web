import { Component, OnInit } from '@angular/core';
import { CertificacionesService } from '../services/certificaciones/certificaciones.service';
import { _fixedSizeVirtualScrollStrategyFactory } from '@angular/cdk/scrolling';
import {Usuario} from '../models/usuario.model';
import {FiltroService} from '../services/filtrar/filtrar.service';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  usuarios: Array<Usuario> = new Array<Usuario>();
  
  constructor(private users: FiltroService, private formBuilder: FormBuilder) {
   
   }

  mostrarSofkianos(){
    this.users.returnUsers().subscribe((usuariosDesdeApi) =>{
      this.usuarios=usuariosDesdeApi;
    })
  }ngOnInit(): void {
    this.mostrarSofkianos();
  }

}
