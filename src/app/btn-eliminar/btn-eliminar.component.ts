import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "app-btn-eliminar",
  templateUrl: "./btn-eliminar.component.html",
  styleUrls: ["./btn-eliminar.component.css"],
})
export class BtnEliminarComponent implements OnInit {
  constructor(private httpClient: HttpClient) {}

  ngOnInit() {
  }

  sendDelete() {
    return this.httpClient
      .delete(
        `http://localhost:8080/api/hojaDeVida//borrarHojaDeVida/`
      )
      .subscribe((data) => console.log(data));
  }
}
