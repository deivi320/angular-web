import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Usuario } from '../../models/usuario.model'

@Injectable({
  providedIn: 'root'
})
export class ApiRegisterService {

  constructor(
    private httpClient: HttpClient
  ) {
  }

  API_URL: string = 'http://localhost:8080/api';

  sendPostRegister(data: Usuario): Observable<Usuario>{
    return this.httpClient.post<Usuario>(
      `${this.API_URL}/user`,
      data)
  }

}
