export interface User {
    uid: string;
    email: string;
    photoURL?: string;
    displayName?: string;
    registered: boolean;
  }