import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Certificacion} from '../../models/certificacion.model';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CertificacionesService {

  constructor(private http: HttpClient) {
  }

  API_URL: string = 'http://localhost:8080/api/';

  leerRegistros(busqueda: string): Observable<Certificacion[]> {
    return this.http.get<Certificacion[]>(`${this.API_URL}certificacion/certificacion/`.concat(busqueda));
  }
}
