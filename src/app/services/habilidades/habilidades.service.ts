import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Habilidades} from '../../models/habilidades.models';

@Injectable({
  providedIn: 'root'
})
export class HabilidadesService {

  constructor(private http: HttpClient) {
  }

  API_URL: string = 'http://localhost:8080/api/';

  leerRegistros(busqueda: string): Observable<Habilidades[]> {
    return this.http.get<Habilidades[]>(`${this.API_URL}habilidades/obtenerHabilidades/`.concat(busqueda));
  }
}
