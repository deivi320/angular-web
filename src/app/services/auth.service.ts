import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';

import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { User } from './user.model';

@Injectable({ providedIn: 'root' })
export class AuthService {
  user$: Observable<User>;

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router
  ) {
    // Get the auth state, then fetch the Firestore user document or return null
    this.user$ = this.afAuth.authState.pipe(
      switchMap(user => {
        // Logged in
        if (user) {
          return this.searchUser(user).valueChanges();
        } else {
          // Logged out
          return of(null);
        }
      })
    );
  }

  async googleSignin() {
    const provider = new auth.GoogleAuthProvider();
    const credential = await this.afAuth.signInWithPopup(provider);
    this.userIsRegistered(credential.user);
  }

  private userIsRegistered(user) {
    const usuarioActual = auth().currentUser;
    if (!usuarioActual.email.includes('@sofka.com.co')) {
      return alert("RECHAZADO: La cuenta con la que está tratando de iniciar sesión no hace parte del domino de sofka.");
    }

    let userResponse = this.searchUser(user).get();
    userResponse.subscribe(userSnapshot => {

      if (!userSnapshot.exists) {
        this.redirectTo('/register');
      } else {
        this.redirectTo('/hojaDeVida');
      }

    });

  }

  private searchUser(user) {
    return this.afs.collection('users').doc<User>(user.uid);
  }

  redirectTo(route) {
    this.router.navigate([route]);
  }

  async signOut() {
    await this.afAuth.signOut().then(() =>{
      this.redirectTo('/');
    }).catch(function (error) {
      console.log('error',error)
    });
   
  }

  changeRegisterState() {
    const currentUser = auth().currentUser;
    const data = this.buildData(currentUser, true);

    this.writeInfoInDB(currentUser, data);
  }

  private buildData(user, registered) {
    return {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      registered: registered
    }
  }

  private writeInfoInDB(user, data) {
    const userRef: AngularFirestoreDocument<User> = this.afs.doc(`users/${user.uid}`);
    userRef.set(data, { merge: true });
  }

}
