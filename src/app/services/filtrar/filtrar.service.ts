import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Usuario} from '../../models/usuario.model';


@Injectable({
  providedIn: 'root'
})
export class FiltroService {
  debugger;
  constructor(private http: HttpClient) {
  }

  API_URL: string = 'http://localhost:8080/api/';
  URLUPDATE:string= 'http://localhost:8080/api/users';

  leerRegistros(busqueda: string): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(`${this.API_URL}search/`.concat(busqueda));
  }

  returnUsers(): Observable<Usuario[]>{
    return this.http.get<Usuario[]>(`${this.API_URL}search/`);
  }


  //metodo para consultar por el uid del usurio
/*  leerUsuario(usuarioId:string):Observable<Usuario>
  {
    return this.http.get<Usuario>('http://localhost:8080/api/usuario/usuarios/'+usuarioId)
  }


  //metodo para actualizar el suario.
  updatePersona(per:Usuario):Observable<Usuario>
  {
    console.log("los datos que se enviaran al back:",per)
    console.log(this.URLUPDATE+"/"+per.numeroDeDocumento,per)
  return this.http.put<Usuario>("http://localhost:8080/api/users/"+per.numeroDeDocumento,per)
  
  }*/
}
