import { TestBed } from '@angular/core/testing';

import { FiltroService } from './filtrar.service';

describe('FiltrarService', () => {
  let service: FiltroService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FiltroService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
