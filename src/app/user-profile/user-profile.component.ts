import { Component, Input } from "@angular/core";
import { AuthService } from "./../services/auth.service";
import { Router } from "@angular/router";
import { Usuario } from "src/app/models/usuario.model";
import { FiltroService } from "../services/filtrar/filtrar.service";
import { User } from "../services/user.model";
@Component({
  selector: "app-user-profile",
  templateUrl: "./user-profile.component.html",
  styleUrls: ["./user-profile.component.css"],
})
export class UserProfileComponent {

  
  usuario: Usuario = new Usuario();

 


  constructor(
    public auth: AuthService,
    private router: Router,
    private usuarioInyectado: FiltroService
  ) {}
}
