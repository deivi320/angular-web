import {Component, OnInit, Input} from '@angular/core';
import {Usuario} from '../models/usuario.model';

@Component({
  selector: 'app-listar-usuarios',
  templateUrl: './listar-usuarios.component.html',
  styleUrls: ['./listar-usuarios.component.css']
})
export class ListarUsuariosComponent implements OnInit {
  @Input() usuarios: Usuario[];
  @Input() pageSize: number;
  @Input() pageNumber: number;

  constructor() {
  }

  ngOnInit(): void {
  }

}
