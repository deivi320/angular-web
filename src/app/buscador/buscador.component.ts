import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { PageEvent } from '@angular/material/paginator';
import { Usuario } from '../models/usuario.model';
import { FiltroService } from '../services/filtrar/filtrar.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.css']
})
export class BuscadorComponent implements OnInit {
  usuarios: Array<Usuario> = new Array<Usuario>();
  usuarioABuscar = '';
  pageSize = 50;
  pageNumber = 1;
  validar: FormGroup;

  constructor(private filtroInyectado: FiltroService, private formBuilder: FormBuilder) {

  }

  ngOnInit() {
    this.validarInput();
  }

  filtrar() {
    this.filtroInyectado.leerRegistros(this.usuarioABuscar).subscribe((usuariosDesdeApi) => {
      this.usuarios = usuariosDesdeApi;
    });
  }

  handlePage(event: PageEvent) {
    this.pageSize = event.pageSize;
    this.pageNumber = event.pageIndex + 1;
  }

  validarInput() {
    this.validar = this.formBuilder.group({
      buscar: ['', Validators.required]
    });
  }
}