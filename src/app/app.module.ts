import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {BuscadorComponent} from './buscador/buscador.component';
import {UserProfileComponent} from './user-profile/user-profile.component';
import {FormularioComponent} from './formulario/formulario.component';
import {PerfilComponent} from './formulario/perfil/perfil.component';
import {HabilidadesComponent} from './formulario/habilidades/habilidades.component';
import {ExperienciasComponent} from './formulario/experiencias/experiencias.component';
import {CertificacionesComponent} from './formulario/certificaciones/certificaciones.component';
import {CursosComponent} from './formulario/cursos/cursos.component';
import {TalleresComponent} from './formulario/talleres/talleres.component';
import {EducacionComponent} from './formulario/educacion/educacion.component';
import {RegisterComponent} from '../app/register/register.component';

import {FiltroService} from './services/filtrar/filtrar.service';

import {PaginacionPipe} from './pipes/paginacion.pipe';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AngularFireModule} from '@angular/fire';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {AngularFireStorageModule} from '@angular/fire/storage';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatPaginatorModule} from '@angular/material/paginator';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { BtnEliminarComponent } from './btn-eliminar/btn-eliminar.component';
import { ListarUsuariosComponent } from './listar-usuarios/listar-usuarios.component';
import { ListarCertificacionesComponent } from './listar-certificaciones/listar-certificaciones.component';
import { ListarHabilidadesComponent } from './listar-habilidades/listar-habilidades.component';
import { BarraNavegacionComponent } from './barra-navegacion/barra-navegacion.component';
import { InicioComponent } from './inicio/inicio.component';
import { DatosPersonalesComponent } from './datos-personales/datos-personales.component';
import { AdminComponent } from './admin/admin.component';
import { FooterComponent } from './footer/footer.component';

const config = {
  apiKey: 'AIzaSyB1To7ctRriPpL1h3i6JMyk4FL7XHZERb4',
  authDomain: 'hoja-de-vida-firebase-login.firebaseapp.com',
  databaseURL: 'https://hoja-de-vida-firebase-login.firebaseio.com',
  projectId: 'hoja-de-vida-firebase-login',
  storageBucket: 'hoja-de-vida-firebase-login.appspot.com',
  messagingSenderId: '1034076994941',
  appId: '1:1034076994941:web:34a49230b88890bc0de014',
  measurementId: 'G-J19NWEHMGB',
};

@NgModule({
  declarations: [
    AppComponent,
    FormularioComponent,
    BuscadorComponent,
    PaginacionPipe,
    UserProfileComponent,
    PerfilComponent,
    TalleresComponent,
    CursosComponent,
    EducacionComponent,
    HabilidadesComponent,
    CertificacionesComponent,
    ExperienciasComponent,
    RegisterComponent,
    BtnEliminarComponent,
    ListarUsuariosComponent,
    ListarCertificacionesComponent,
    ListarHabilidadesComponent,
    BarraNavegacionComponent,
    InicioComponent,
    DatosPersonalesComponent,
    AdminComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(config),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    MatPaginatorModule,
    BrowserAnimationsModule
  ],

  providers: [
    FiltroService
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
