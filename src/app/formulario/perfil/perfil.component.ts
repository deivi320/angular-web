import { Component, OnInit } from "@angular/core";
import { FormControl, Validators, FormGroup } from "@angular/forms";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { AngularFireAuth } from "@angular/fire/auth/";


@Component({
  selector: "app-perfil",
  templateUrl: "./perfil.component.html",
  styleUrls: ["./perfil.component.css"],
})
export class PerfilComponent implements OnInit {
  userId: string = "";
  perfiles: FormGroup;
  perfil: FormControl;

  crearFormsControls() {
    this.perfil = new FormControl("");
  }

  crearPerfiles() {
    this.perfiles = new FormGroup({
      perfil: this.perfil,
    });
  }
  constructor(private httpClient: HttpClient, public auth: AngularFireAuth) {}

  ngOnInit() {
    this.crearFormsControls();
    this.crearPerfiles();
    this.auth.authState.subscribe((user) => {
      if (user) {
        this.userId = user.uid;
      }
    });
  }

  sendPostRequest(data: any): Observable<any> {
    return this.httpClient.post<any>("http://localhost:8080/api/perfil", data);
  }

  onSubmit() {
    if (this.perfiles.valid) {
      this.sendPostRequest({
        usuarioId: this.userId,
        descripcion: this.perfil.value,
      }).subscribe((res) => {
        console.info(res);
      });
      this.perfiles.reset();
    }
  }
}
