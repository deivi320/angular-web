import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { AngularFireAuth } from "@angular/fire/auth/";

interface Icertificacion {
  usuarioId: string,
  idCertificacion: string,
  nombre: string,
  institucion: string,
  periodo: string
}

@Component({
  selector: "app-certificaciones",
  templateUrl: "./certificaciones.component.html",
  styleUrls: ["./certificaciones.component.css"],
})
export class CertificacionesComponent implements OnInit {
  userId: string = "";
  certificaciones: FormGroup;
  certificacion: FormControl;
  institucion: FormControl;
  fecha: FormControl;
  certificacionesArray: Array<Icertificacion> = [];

  createFormControls() {
    this.certificacion = new FormControl("", Validators.required);
    this.institucion = new FormControl("", Validators.required);
    this.fecha = new FormControl("");
  }

  createForm() {
    this.certificaciones = new FormGroup({
      certificacion: this.certificacion,
      institucion: this.institucion,
      fecha: this.fecha,
    });
  }

  ngOnInit() {
    this.createFormControls();
    this.createForm();
    this.auth.authState.subscribe((user) => {
      if (user) {
        this.userId = user.uid;
      }
      this.sendGetReq()
        .subscribe((data: any[]) =>
          this.certificacionesArray = data);
    });
  }

  sendGetReq() {
    return this.httpClient.get("http://localhost:8080/api/certificacion/certificacion/" + this.userId);
  }

  constructor(private httpClient: HttpClient, public auth: AngularFireAuth) { }

  sendPostRequest(data: any): Observable<any> {
    return this.httpClient.post<any>(
      "http://localhost:8080/api/certificacion/certificacion",
      data
    );
  }

  displayCursos(res: any) {
    let certificacionNew: Icertificacion = {
      usuarioId: res.usuarioId,
      idCertificacion: res.idCertificacion,
      nombre: res.nombre,
      institucion: res.institucion,
      periodo: res.periodo
    }
    this.certificacionesArray.push(certificacionNew);
  }

  eliminarCertificacion(idCertificacion: any) {
    this.certificacionesArray = this.certificacionesArray
      .filter(cert => cert.idCertificacion !== idCertificacion);
    this.sendDeleteReq(idCertificacion).subscribe();
  }

  sendDeleteReq(idCertificacion: any) {
    return this.httpClient.delete("http://localhost:8080/api/certificacion/borrarCertificacion/" + idCertificacion);
  }

  onSubmit() {
    if (this.certificaciones.valid) {
      this.sendPostRequest({
        nombre: this.certificacion.value,
        institucion: this.institucion.value,
        periodo: this.fecha.value,
        usuarioId: this.userId,
      }).subscribe((res) => {
        console.info(res);
        this.displayCursos(res);
      });
      this.certificaciones.reset();
    }
  }
}
