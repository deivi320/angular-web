import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { AngularFireAuth } from "@angular/fire/auth/";

@Component({
  selector: "app-talleres",
  templateUrl: "./talleres.component.html",
  styleUrls: ["./talleres.component.css"],
})
export class TalleresComponent implements OnInit {
  todosLosTalleres: Array<any> = [];
  userId: string = "";
  talleres: FormGroup;
  nombreTaller: FormControl;
  nombreInstitucion: FormControl;

  createFormControls() {
    this.nombreTaller = new FormControl("");
    this.nombreInstitucion = new FormControl("");
  }

  createForm() {
    this.talleres = new FormGroup({
      nombreTaller: this.nombreTaller,
      nombreInstitucion: this.nombreInstitucion,
    });
  }

  ngOnInit() {
    this.createFormControls();
    this.createForm();
    this.auth.authState.subscribe((user) => {
      if (user) {
        this.userId = user.uid;
        this.sendGetRequest();
      }
    });
  }

  constructor(private httpClient: HttpClient, public auth: AngularFireAuth) {}

  sendPostRequest(data: any): Observable<any> {
    return this.httpClient.post<any>(
      "http://localhost:8080/api/talleres/taller",
      data
    );
  }

  sendGetRequest() {
    return this.httpClient
      .get(`http://localhost:8080/api/talleres/${this.userId}`)
      .subscribe((data: any[]) => {
        this.todosLosTalleres = data;
      });
  }

  eliminarTaller(idTaller) {
    return this.httpClient
      .delete(`http://localhost:8080/api/talleres/delete/${idTaller}`)
      .subscribe();
  }

  onSubmit() {
    if (this.talleres.valid) {
      this.sendPostRequest({
        nombre: this.nombreTaller.value,
        institucion: this.nombreInstitucion.value,
        usuarioId: this.userId,
      }).subscribe((res) => {
        console.info(res);
      });
      this.talleres.reset();
      this.ngOnInit();
    }
  }

  onClick(idTaller) {
    this.todosLosTalleres = this.todosLosTalleres.filter(
      (taller) => taller.idTaller !== idTaller
    );
    this.eliminarTaller(idTaller);
  }
}
