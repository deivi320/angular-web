import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { AngularFireAuth } from "@angular/fire/auth/";

interface Iexperiencia {
  usuarioId;
  idExperiencia;
  nombreEmpresa;
  descripcion;
  cargo;
  fechaInicio;
  fechaFin;
}

@Component({
  selector: "app-experiencias",
  templateUrl: "./experiencias.component.html",
  styleUrls: ["./experiencias.component.css"],
})
export class ExperienciasComponent implements OnInit {
  userId: string = "";
  experiencias: FormGroup;
  empresa: FormControl;
  cargo: FormControl;
  descripcion: FormControl;
  fechaInicio: FormControl;
  fechaFin: FormControl;
  experienciasArray: Array<Iexperiencia> = [];

  crearExperienciasControl() {
    this.empresa = new FormControl("");
    this.cargo = new FormControl("");
    this.descripcion = new FormControl("");
    this.fechaInicio = new FormControl("");
    this.fechaFin = new FormControl("");
  }

  crearExperiencias() {
    this.experiencias = new FormGroup({
      empresa: this.empresa,
      cargo: this.cargo,
      descripcion: this.descripcion,
      fechaInicio: this.fechaInicio,
      fechaFin: this.fechaFin,
    });
  }

  ngOnInit() {
    this.crearExperienciasControl();
    this.crearExperiencias();
    this.auth.authState.subscribe((user) => {
      if (user) {
        this.userId = user.uid;
      }
      this.sendGetRequest().subscribe((data: any[]) => {
        this.experienciasArray = data;
      });
    });
  }

  sendGetRequest() {
    return this.httpClient.get(
      "http://localhost:8080/api/experiencia/experiencias/" + this.userId
    );
  }

  constructor(private httpClient: HttpClient, public auth: AngularFireAuth) {}

  sendPostRequest(data: any): Observable<any> {
    return this.httpClient.post<any>(
      "http://localhost:8080/api/experiencia/guardarExperiencia",
      data
    );
  }

  eliminarExperiencia(idExperiencia: any) {
    this.experienciasArray = this.experienciasArray.filter(
      (exp) => exp.idExperiencia !== idExperiencia
    );
    this.sendDeleteRequest(idExperiencia).subscribe();
  }

  sendDeleteRequest(idExperiencia: any): Observable<any> {
    return this.httpClient.delete(
      `http://localhost:8080/api/experiencia/experiencia/${idExperiencia}`
    );
  }

  displayExperiencias(res: any) {
    let experiencia: Iexperiencia = {
      usuarioId: res.usuarioId,
      idExperiencia: res.idExperiencia,
      nombreEmpresa: res.nombreEmpresa,
      descripcion: res.descripcion,
      cargo: res.cargo,
      fechaInicio: res.fechaInicio,
      fechaFin: res.fechaFin,
    };
    this.experienciasArray.push(experiencia);
  }

  onSubmit() {
    if (this.experiencias.valid) {
      this.sendPostRequest({
        usuarioId: this.userId,
        nombreEmpresa: this.empresa.value,
        descripcion: this.descripcion.value,
        cargo: this.cargo.value,
        fechaInicio: this.fechaInicio.value,
        fechaFin: this.fechaFin.value,
      }).subscribe((res) => {
        console.info(res);
        this.displayExperiencias(res);
        this.experiencias.reset();
      });
    }
  }
}
