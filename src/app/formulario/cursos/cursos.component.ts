import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { AngularFireAuth } from "@angular/fire/auth/";

interface Icursos {
  idCurso: string,
  nombre: string,
  institucion: string
}

@Component({
  selector: "app-cursos",
  templateUrl: "./cursos.component.html",
  styleUrls: ["./cursos.component.css"],
})
export class CursosComponent implements OnInit {
  userId: string = "";
  cursos: FormGroup;
  nombreCurso: FormControl;
  institucion: FormControl;
  cursosArray: Array<Icursos> = [];

  createFormControls() {
    this.nombreCurso = new FormControl("");
    this.institucion = new FormControl("");
  }

  createFormGroup() {
    this.cursos = new FormGroup({
      nombreCurso: this.nombreCurso,
      institucion: this.institucion,
    });
  }

  ngOnInit() {
    this.createFormControls();
    this.createFormGroup();
    this.auth.authState.subscribe((user) => {
      if (user) {
        this.userId = user.uid;
      }
      this.sendGetRequest(this.userId).subscribe((data: any[]) => this.cursosArray = data);
    });
  }

  sendGetRequest(id: any) {
    return this.httpClient.get("http://localhost:8080/api/cursos/getAll/" + this.userId);
  }

  constructor(private httpClient: HttpClient, public auth: AngularFireAuth) { }

  sendPostRequest(data: any): Observable<any> {
    return this.httpClient.post<any>(
      "http://localhost:8080/api/cursos/save",
      data
    );
  }

  eliminarCurso(idCurso: any) {
    this.cursosArray = this.cursosArray.filter(
      (cur) => cur.idCurso !== idCurso);
    this.sendDeleteRequest(idCurso).subscribe();
  }

  sendDeleteRequest(id: any) {
    return this.httpClient.delete("http://localhost:8080/api/cursos/delete/" + id)
  }

  displayCursos(res: any) {
    let cursoNew: Icursos = {
      idCurso: res.idCurso,
      nombre: res.nombre,
      institucion: res.institucion
    }
    this.cursosArray.push(cursoNew);
  }

  onSubmit() {
    if (this.cursos.valid) {
      this.sendPostRequest({
        nombre: this.nombreCurso.value,
        institucion: this.institucion.value,
        usuarioId: this.userId,
      }).subscribe((res) => {
        console.info(res);
        this.displayCursos(res);
      });
      this.cursos.reset();
    }
  }
}
