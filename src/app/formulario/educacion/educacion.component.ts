import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { AngularFireAuth } from "@angular/fire/auth/";

@Component({
  selector: "app-educacion",
  templateUrl: "./educacion.component.html",
  styleUrls: ["./educacion.component.css"],
})
export class EducacionComponent implements OnInit {
  educaciones: Array<any> = [];
  userId: string = "";
  educacion: FormGroup;
  institucion: FormControl;
  ciudad: FormControl;
  titulo: FormControl;
  fechaInicio: FormControl;
  fechaFin: FormControl;

  crearEducacionControl() {
    this.institucion = new FormControl("");
    this.ciudad = new FormControl("");
    this.titulo = new FormControl("");
    this.fechaInicio = new FormControl("");
    this.fechaFin = new FormControl("");
  }

  crearEducacion() {
    this.educacion = new FormGroup({
      institucion: this.institucion,
      ciudad: this.ciudad,
      titulo: this.titulo,
      fechaInicio: this.fechaInicio,
      fechaFin: this.fechaFin,
    });
  }

  ngOnInit() {
    this.crearEducacionControl();
    this.crearEducacion();
    this.auth.authState.subscribe((user) => {
      if (user) {
        this.userId = user.uid;
        this.sendGetRequest();
      }
    });
  }

  constructor(private httpClient: HttpClient, public auth: AngularFireAuth) {}

  sendPostRequest(data: any): Observable<any> {
    return this.httpClient.post<any>(
      "http://localhost:8080/api/educacion/",
      data
    );
  }

  sendGetRequest() {
    return this.httpClient
      .get(`http://localhost:8080/api/educacion/${this.userId}`)
      .subscribe((data: any[]) => {
        this.educaciones = data;
      });
  }

  sendDeleteRequest(educacionId: any) {
    return this.httpClient.delete(
      `http://localhost:8080/api/educacion/eliminar/${educacionId}`
    );
  }

  onSubmit() {
    if (this.educacion.valid) {
      this.sendPostRequest({
        nombreInstitucion: this.institucion.value,
        titulo: this.titulo.value,
        ciudad: this.ciudad.value,
        fechaInicio: this.fechaInicio.value,
        fechaFin: this.fechaFin.value,
        usuarioId: this.userId,
      }).subscribe((res) => console.info(res));

      this.educacion.reset();
      this.ngOnInit();
    }
  }

  onClick(educacionId) {
    this.educaciones = this.educaciones.filter(
      (educacion) => educacion.educacionId !== educacionId
    );
    this.sendDeleteRequest(educacionId).subscribe();
  }
}
