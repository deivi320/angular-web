import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { AngularFireAuth } from "@angular/fire/auth/";

interface habilidadSofkiano {
  id: string;
  categoria: string;
  habilidad: string;
}

@Component({
  selector: "app-habilidades",
  templateUrl: "./habilidades.component.html",
  styleUrls: ["./habilidades.component.css"],
})
export class HabilidadesComponent implements OnInit {
  userId: string = "";
  habilidad: FormGroup;
  categoria: FormControl;
  habilidadItem: FormControl;
  habilidadesIngresadas: Array<habilidadSofkiano> = [];

  crearHabilidadControls() {
    this.categoria = new FormControl("");
    this.habilidadItem = new FormControl("");
  }

  crearHabilidades() {
    this.habilidad = new FormGroup({
      categoria: this.categoria,
      item: this.habilidadItem,
    });
  }

  ngOnInit() {
    this.crearHabilidadControls();
    this.crearHabilidades();
    this.auth.authState.subscribe((user) => {
      if (user) {
        this.userId = user.uid;
      }
      this.sendGetReq().subscribe((data: any[]) => {
        this.habilidadesIngresadas = data;
      });
    });
  }

  sendGetReq() {
    return this.httpClient.get(
      "http://localhost:8080/api/habilidades/obtenerHabilidades/" + this.userId
    );
  }

  constructor(private httpClient: HttpClient, public auth: AngularFireAuth) { }

  sendPostRequest(data: any): Observable<any> {
    return this.httpClient.post<any>(
      "http://localhost:8080/api/habilidades/guardarHabilidad",
      data
    );
  }

  sendDeleteRequest(idHabilidad: any): Observable<any> {
    return this.httpClient.delete(
      "http://localhost:8080/api/habilidades/borrarHabilidad/" + idHabilidad
    );
  }

  displayHabilities(res: any) {
    let habilidad: habilidadSofkiano = {
      categoria: res.categoria,
      habilidad: res.habilidad,
      id: res.id,
    };
    this.habilidadesIngresadas.push(habilidad);
  }

  eliminarHabilidad(idHabilidad: any) {
    this.habilidadesIngresadas = this.habilidadesIngresadas.filter(
      (hab) => hab.id !== idHabilidad
    );
    this.sendDeleteRequest(idHabilidad).subscribe();
  }

  onSubmit() {
    if (this.habilidad.valid) {
      let categoria: string = this.categoria.value;
      let item: string = this.habilidadItem.value;
      this.sendPostRequest({
        usuarioId: this.userId,
        categoria: categoria.toLowerCase(),
        habilidadItem: item.toLowerCase(),
      }).subscribe((res) => {
        console.info(res);
        this.displayHabilities(res);
        this.habilidad.reset();
      });
    }
  }
}
