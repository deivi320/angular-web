import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {ApiRegisterService} from '../services/registrar/api-register.service';
import {AngularFireAuth} from '@angular/fire/auth/';
import { Router } from '@angular/router'
import { AuthService } from '../services/auth.service'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  private userId;
  private ROL = 'Colaborador';
  esNuevo: boolean = true;

  generos: string[] = ['Masculino', 'Femenino', 'Otro'];
  tiposDeDocumento: string[] = [
    'Tarjeta de identidad',
    'Cédula de ciudadanía',
    'Certificado de extranjería',
  ];
  register: FormGroup;
  nombre: FormControl;
  apellido: FormControl;
  numeroDeDocumento: FormControl;
  tipoDeDocumento: FormControl;
  edad: FormControl;
  telefono: FormControl;
  genero: FormControl;
  correo: FormControl;

  createRegisterControls() {
    this.nombre = new FormControl('', Validators.required);
    this.apellido = new FormControl('', Validators.required);
    this.numeroDeDocumento = new FormControl('', Validators.required);
    this.tipoDeDocumento = new FormControl('', Validators.required);
    this.edad = new FormControl('', Validators.required);
    this.telefono = new FormControl('', [
      Validators.required,
      Validators.maxLength(15),
      Validators.minLength(7),
    ]);
    this.genero = new FormControl('', Validators.required);
    this.correo = new FormControl('', [
      Validators.required,
      Validators.pattern('[A-Za-z0-9.%-]+@[A-Za-z0-9.%-]+\\.[a-z]{2,3}'),
    ]);
  }

  createRegisterForm() {
    this.register = new FormGroup({
      nombre: this.nombre,
      apellido: this.apellido,
      numeroDeDocumento: this.numeroDeDocumento,
      tipoDeDocumento: this.tipoDeDocumento,
      edad: this.edad,
      genero: this.genero,
      telefono: this.telefono,
      correo: this.correo,
    });
  }

  constructor(
    private _authService: AuthService,
    private _registerService: ApiRegisterService,
    private _auth: AngularFireAuth,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.createRegisterControls(), this.createRegisterForm();
    this._auth.authState.subscribe((user) => {
      if (user) {
        this.userId = user.uid;
        this.correo.setValue(user.email);
      }
    });
  }

  onSubmit() {
    if (this.userId) {
      if (!this.register.valid) {
        console.log('Invalid register');
      } else {
        this._registerService
          .sendPostRegister({
            usuarioId: this.userId,
            nombre: this.nombre.value,
            apellido: this.apellido.value,
            genero: this.genero.value,
            edad: this.edad.value,
            correo: this.correo.value,
            numeroDeTelefono: this.telefono.value,
            numeroDeDocumento: this.numeroDeDocumento.value,
            tipoDeDocumento: this.tipoDeDocumento.value,
            rol: this.ROL,
          })
          .subscribe((response) => {
            console.info(response);
            this._authService.changeRegisterState();
          });
        this.register.reset();
        this.router.navigate(['/hojaDeVida']);
      }
    } else {
      alert('No se puede registrar.');
    }
  }
}
